# Rupell
Rupell is an open source shell built in Rust as a hobby project. The main goals of this project are to make a: cross-platform shell, that is beautiful, safe but powerful, and free (as in freedom)!

# *How to Install/Update on UNIX/UNIX-Like Systems*
Method 1:
  - You can use the built-in `rupell-plant` command to both install from running the source code with `cargo run`, or to update by running it directly from the program, and restarting Rupell.

Method 2:
  - Run the command below, to install or update.
  ```
  git clone https://codeberg.org/Oglo12/rupell.git && cd rupell && cargo build --release && sudo mv target/release/rupell /usr/bin/ && cd .. && rm -rf rupell/ && echo "" && echo "All done! :-)" && echo "You may now use the \"rupell\" command to use Rupell!"
  ```

# *How to Use Rupell*
To get started using Rupell, use the `help` command to see different commands!

# *Tips & Tricks*
> command chains

You can use commands one by one, or you can chain them! By using the `&C` operator in your command, you can split one command into more commands!

Before:
```
echo "Help List:"
help
```

After:
```
echo "Help List:" &C help
```

---
> command: help

You can use `help` without any arguments to just get a list of commands with small descriptions, but you can also use any command as an argument to get a more in-depth description on the command in question!

---
> command: echo

You can print a string normally with `echo`, but you can also write to a file with `echo`!

Add a Line: `echo "YOUR STRING HERE" -> YOUR_FILE`

Erase Then Add: `echo "YOUR STRING HERE" !> YOUR_FILE`

# *Special Operator -> String Converter || Cheatsheet*
> operator | string version

>     `&C` = `&c` # Command Split Operator

>      `"` = `\'` # String Operator
