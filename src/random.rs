/*
Rupell - A pretty terminal wrapper built in Rust.
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use colored::Colorize;
use rand::Rng;

pub fn random_color(text: &str) -> String {
    let rgb_favor = random(0, 6) as i32;

    let (r, g, b) = random_color_core(rgb_favor);

    let result = text.truecolor(r, g, b).to_string();

    return result;
}

pub fn random_controlled_color(text: &str, family: &str) -> String {
    let mut rgb_favor: i32 = 0;

    let family_l = family.to_lowercase();

    if family_l == "r" {
        rgb_favor = 0;
    } else if family_l == "g" {
        rgb_favor = 1;
    } else if family_l == "b" {
        rgb_favor = 2;
    } else if family_l == "y" {
        rgb_favor = 3;
    } else if family_l == "p" {
        rgb_favor = 4;
    } else if family_l == "t" {
        rgb_favor = 5;
    }

    let (r, g, b) = random_color_core(rgb_favor);

    let result = text.truecolor(r, g, b).to_string();

    return result;
}

pub fn random(n1: isize, n2: isize) -> isize {
    return rand::thread_rng().gen_range(n1..n2);
}

pub fn random_color_core(family: i32) -> (u8, u8, u8) {
    let rgb_favor: i32;

    if family > 5 {
        rgb_favor = 5;
    } else if family < 0 {
        rgb_favor = 0;
    } else {
        rgb_favor = family;
    }

    let mut r: u8 = 0;
    let mut g: u8 = 0;
    let mut b: u8 = 0;

    if rgb_favor == 0 {
        r = random(200, 256) as u8;
        g = random(100, 200) as u8;
        b = random(100, 200) as u8;
    } else if rgb_favor == 1 {
        g = random(200, 256) as u8;
        b = random(100, 200) as u8;
        r = random(100, 200) as u8;
    } else if rgb_favor == 2 {
        b = random(200, 256) as u8;
        r = random(100, 200) as u8;
        g = random(100, 200) as u8;
    } else if rgb_favor == 3 {
        r = random(200, 256) as u8;
        g = random(200, 256) as u8;
        b = random(100, 200) as u8;
    } else if rgb_favor == 4 {
        r = random(200, 256) as u8;
        g = random(100, 200) as u8;
        b = random(200, 256) as u8;
    } else if rgb_favor == 5 {
        r = random(100, 200) as u8;
        g = random(200, 256) as u8;
        b = random(200, 256) as u8;
    }

    return (r, g, b);
}
