/*
Rupell - A pretty terminal wrapper built in Rust.
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::env;
use std::process::Command;

pub const OS: &str = env::consts::OS;

pub fn native_command(command: &str) {
    if OS == "windows" {
        Command::new("cmd").args(["/C", command]).status().unwrap();
    } else {
        Command::new("bash").args(["-c", command]).status().unwrap();
    }
}
