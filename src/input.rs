/*
Rupell - A pretty terminal wrapper built in Rust.
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::io;
use std::io::Write;

pub fn input(text: &str, sep: bool) -> String {
    let mut prompt = String::new();
    if sep == true {
        print!("{} ", text);
    } else {
        print!("{}", text);
    }
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut prompt)
        .expect("Failed to read line!");
    return prompt.trim().to_string();
}
