/*
Rupell - A pretty terminal wrapper built in Rust.
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Modules
mod changers_reset;
mod input;
mod misc;
mod populate_fs_read;
mod random;
mod your_os;

// Use Statements
use colored::Colorize;
use input::input;
use misc::*;
use populate_fs_read::*;
use random::*;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use your_os::*;

// Constants
const SPLIT: &str = "$%&*^&!$$";
const OS_WARN: &str =
    "It seems you are not on a UNIX/UNIX-like OS, you will have a limited experience!";
const GIT_REPO_LINK: &str = "https://codeberg.org/Oglo12/rupell";

fn main() {
    // Conditional Variables
    let on_unix_gnu: bool;
    if OS != "windows" {
        on_unix_gnu = true;
    } else {
        on_unix_gnu = false;
    }

    // Getting Username
    let username: String;
    if on_unix_gnu == true {
        username = get_username_unix_gnu();
    } else {
        username = String::from("null");
    }

    // Command Variables
    let calls: Vec<(&str, &str, &str)> = vec![
        ("help", "Gives you a list of commands with their functions.", "Brings up a list of commands with their functions, you can get in depth descriptions by using the command in question as an argument."),
        ("exit", "Quits the app.", "Quits the app by breaking out of the program loop."),
        ("about-system", "Tells you about the OS and more.", "Gives you info on what OS (operating system) you are running along with other info, like your system architecture."),
        ("echo", "Repeats what you say.", "Prints out exactly what you typed in, or adds lines to a file."),
        ("cat", "Prints out the contents of a file.", "Prints out every single line in a text file in order."),
        ("special-help", "Gives you a list of special commands, and what they do.", "Brings up a list of special commands with what they do, note that this command isn't as powerful as the other help command for regular commands! You cannot use commands as arguments!"),
        ("rupell-plant", "Installs or updates Rupell.", "Pulls down the Rupell Git repo temporarily to move/overwrite the Rupell binary in /usr/bin on UNIX and UNIX-like systems. Does not work on Windows!"),
        ("warn", "Repeats what you say as a warning.", "Prints out exactly what you typed in, but using the same function Rupell uses to print out warnings."),
        ("error", "Repeats what you say as an error.", "Prints out exactly what you typed in, but using the same function Rupell uses to print out errors."),
        ("success", "Repeats what you say as a success message.", "Prints out exactly what you typed in, but using the same function Rupell uses to print out success messages."),
        ("alias", "Makes command shortcuts.", "Creates a fake command that when run, actually executes a specific command."),
        ("alias-list", "Lists all created aliases.", "Prints a list of all aliases created in the current Rupell instance, along with their execution commands.")
    ];

    let special_calls: Vec<(&str, &str)> = vec![
        (">>", "Calls a command from your native OS shell."),
        ("//", "Runs an external Rupell script."),
    ];

    // Aliases
    let mut aliases: Vec<(String, String)> = Vec::new();

    // Loop Changers
    let mut script = changers_reset::script();

    // RC File Stuff
    if on_unix_gnu == true {
        let rc_file = populate_read_to_string(format!("/home/{}/.rupellrc", username).as_str(), "# The Rupell RC File :-)\necho \"<~~~~ Welcome to Rupell! ~~~~>\"\necho \"\"\necho \"Here are some commands!\"\necho \" Regular Commands:\"\nhelp\necho \"\"\necho \" Special Commands:\"\nspecial-help\necho \"\"\necho \"Here is the GitHub if you want to help the project!\"\necho \"https://github.com/Oglo12/rupell/\"\necho \"\"");

        script.0 = true;
        script.4 = true;

        for i in rc_file.split("\n") {
            script.1.push(i.to_string());
        }
    } else {
        warn(OS_WARN);
    }

    // The Main Loop
    loop {
        // Variables
        let mut multi_command_behavior = false;
        let mut command: Vec<String> = Vec::new();
        let mut user_input: String;

        // Reset Script
        if script.2 == script.1.len() {
            script = changers_reset::script();
        }

        // Get user/script input.
        if script.0 == false {
            user_input = input("Command:".blue().bold().to_string().as_str(), true);
        } else {
            user_input = script.1[script.2].to_string();
            script.2 += 1;
        }

        user_input = user_input.trim().to_string();

        //// Cutter ////
        // Stage 0: Multi-Command Parsing
        let mut multi_parse: Vec<String> = Vec::new();
        for i in user_input.split(" &C ") {
            multi_parse.push(i.trim().to_string());
        }

        if multi_parse.len() == 1 {
            // Do nothing, you're good! :-)
        } else {
            for i in multi_parse.iter() {
                script.1.push(i.to_string());
            }

            script.0 = true;

            user_input = String::from("");

            multi_command_behavior = true;
        }

        let mut phrase_vec: Vec<String> = Vec::new();
        for i in user_input.split(" &c ") {
            phrase_vec.push(i.to_string());
        }
        user_input = string_vec_to_string(&phrase_vec, " &C ", 0, phrase_vec.len());

        // Stage 1: Commenting
        if user_input.starts_with("#") {
            user_input = String::from("");
        }

        // Stage 2: Prompt Strings
        let mut cut = true;
        let mut phrase = String::new();
        for i in user_input.chars() {
            if i == '\"' {
                cut = !cut;
            }

            if cut == true {
                if i == ' ' {
                    phrase.push_str(SPLIT);
                } else {
                    if i != '"' {
                        phrase.push(i);
                    }
                }
            } else {
                if i != '"' {
                    phrase.push(i);
                }
            }
        }

        // Stage 3: Turning "\'" into """ that the user can use!
        let mut final_phrase = String::new();
        let mut vec_final_phrase: Vec<String> = Vec::new();
        for i in phrase.split("\\'") {
            vec_final_phrase.push(i.to_string());
        }
        for i in 0..vec_final_phrase.len() {
            final_phrase.push_str(vec_final_phrase[i].as_str());
            if i != vec_final_phrase.len() - 1 {
                final_phrase.push_str("\"");
            }
        }

        // Stage 4: Splitting into the command vector!
        for i in final_phrase.split(SPLIT) {
            command.push(i.to_string());
        }

        let mut valid_command = true;

        if char_count(user_input.as_str(), '"') % 2 == 0 {
            // Pass, it is okay! :-)
        } else {
            valid_command = false;
        }

        //// Print The Command ////
        if script.3 == true && multi_command_behavior == false {
            println!(
                "  {} {}",
                "Running...".truecolor(100, 100, 100),
                user_input.cyan().bold()
            );
        }

        // Command Query and Execution
        if valid_command == true {
            if command[0] == calls[0].0 {
                let valid_lengths: Vec<usize> = vec![1, 2];
                let valid = check_length(valid_lengths, &command);
                if valid == true {
                    if command.len() == 1 {
                        let mut pos: usize = 1;
                        for i in calls.iter() {
                            println!(
                                "  {}{} {}{} {}",
                                pos.to_string().cyan().bold(),
                                ".".truecolor(100, 100, 100),
                                i.0.green(),
                                ":".truecolor(100, 100, 100),
                                i.1.cyan()
                            );

                            pos += 1;
                        }
                        println!("  {}", random_controlled_color(format!("Use \"{} <insert command here>\" to see more details on one command.", calls[0].0).as_str(), "p"));
                    } else if command.len() == 2 {
                        let mut found = false;
                        for i in calls.iter() {
                            if command[1].as_str() == i.0 {
                                found = true;
                                for _i in 0..5 {
                                    print!("{}", "-".bold().truecolor(100, 100, 100));
                                }
                                print!("{}", "~| ".bold().truecolor(100, 100, 100));
                                print!("{}", i.0.cyan().bold());
                                print!("{}", " |~".bold().truecolor(100, 100, 100));
                                for _i in 0..5 {
                                    print!("{}", "-".bold().truecolor(100, 100, 100));
                                }
                                print!("\n");
                                println!("{} {}", random_color("█"), random_color(i.2));
                                for _i in 0..(16 + i.0.len()) {
                                    print!("{}", "-".bold().truecolor(100, 100, 100));
                                }
                                print!("\n");
                                break;
                            }
                        }

                        if found == false {
                            error("Could not find an entry for that command!");
                        }
                    }
                }
            } else if command[0] == calls[1].0 {
                let vls: Vec<usize> = vec![1];
                let valid = check_length(vls, &command);
                if valid == true {
                    println!("{}", "Goodbye, I am sad to see you go. (T_T)".yellow());
                    break;
                }
            } else if command[0] == calls[2].0 {
                let vls: Vec<usize> = vec![1];
                let valid = check_length(vls, &command);
                if valid == true {
                    let sys_info: Vec<(&str, &str)> = vec![
                        ("Operating System", env::consts::OS),
                        ("Architecture", env::consts::ARCH),
                    ];

                    for i in sys_info.iter() {
                        println!(
                            "  {}{} {}",
                            random_controlled_color(i.0, "g"),
                            ":".truecolor(100, 100, 100),
                            random_controlled_color(i.1, "y")
                        );
                    }
                }
            } else if command[0] == calls[3].0 {
                let vls: Vec<usize> = vec![2, 4];
                let valid = check_length(vls, &command);
                if valid == true {
                    if command.len() == 2 {
                        println!("{}", command[1]);
                    } else if command.len() == 4 {
                        if command[2] == "->" {
                            let valid: bool = match File::open(command[3].to_string()) {
                                Ok(_o) => true,
                                Err(_e) => {
                                    error("That file does not exist!");
                                    false
                                }
                            };

                            if valid == true {
                                let mut file = File::open(command[3].to_string())
                                    .expect("could not open file... bad programming :-(");
                                let mut data_string = String::new();
                                file.read_to_string(&mut data_string)
                                    .expect("failed to read file");
                                let mut data: Vec<String> = Vec::new();
                                for i in data_string.split("\n") {
                                    data.push(i.to_string());
                                }
                                data.push(command[1].to_string());
                                data_string = string_vec_to_string(&data, "\n", 0, data.len());
                                let mut file = File::create(command[3].to_string())
                                    .expect("failed to create file");
                                file.write_all(data_string.as_bytes())
                                    .expect("failed to create/write file");
                            }
                        } else if command[2] == "!>" {
                            let mut proceed = bool_question(format!("This will replace the entire file with \"{}\", do you want to proceed?", command[1]).as_str());
                            if proceed == true {
                                proceed = bool_question("Are you sure?");
                                if proceed == true {
                                    let valid = match File::open(command[3].to_string()) {
                                        Ok(_o) => true,
                                        Err(_e) => {
                                            error("That file does not exist!");
                                            false
                                        }
                                    };
                                    if valid == true {
                                        let mut file = File::create(command[3].to_string())
                                            .expect("failed to create/write file");
                                        file.write_all(command[1].as_bytes())
                                            .expect("failed to write to file");
                                    }
                                } else {
                                    println!("{}", "Aborting!".yellow());
                                }
                            } else {
                                println!("{}", "Aborting!".yellow());
                            }
                        } else {
                            error("Invalid argument!");
                        }
                    }
                }
            } else if command[0] == calls[4].0 {
                let vls: Vec<usize> = vec![2];
                let valid = check_length(vls, &command);
                if valid == true {
                    let proceed: bool = match File::open(command[1].to_string()) {
                        Ok(_o) => true,
                        Err(_e) => {
                            error("That file does not exist!");
                            false
                        }
                    };

                    if proceed == true {
                        let mut file = File::open(command[1].to_string())
                            .expect("failed to open file... bad programming");
                        let fancy: bool;
                        if script.4 == true {
                            fancy = false;
                        } else {
                            fancy = bool_question("Would you like to print in fancy text?");
                        }
                        if fancy == true {
                            let mut data_string = String::new();
                            file.read_to_string(&mut data_string)
                                .expect("failed to read to string");
                            let mut data: Vec<String> = Vec::new();
                            for i in data_string.split("\n") {
                                data.push(i.to_string());
                            }
                            let mut use_sep = true;
                            let mut position = 0;
                            loop {
                                if use_sep == true {
                                    print!("{}", "───┼".truecolor(70, 70, 70).bold());
                                    for _i in 0..(terminal_size().0 - 4) {
                                        print!("{}", "─".truecolor(70, 70, 70).bold());
                                    }
                                    print!("\n");

                                    if position == data.len() {
                                        break;
                                    }
                                } else {
                                    print!(
                                        "{}{}",
                                        (position + 1).to_string().bold().cyan(),
                                        ". │".truecolor(70, 70, 70).bold()
                                    );
                                    println!("{}", data[position]);
                                    position += 1;
                                }

                                use_sep = !use_sep;
                            }
                        } else {
                            let mut data_string = String::new();
                            file.read_to_string(&mut data_string)
                                .expect("failed to read to string");
                            println!("{}", data_string);
                        }
                    }
                }
            } else if command[0] == calls[5].0 {
                let vls: Vec<usize> = vec![1];
                let valid = check_length(vls, &command);
                if valid == true {
                    let mut pos: usize = 1;
                    for i in special_calls.iter() {
                        println!(
                            "  {}{} {}{} {}",
                            pos.to_string().cyan().bold(),
                            ".".truecolor(100, 100, 100),
                            i.0.green(),
                            ":".truecolor(100, 100, 100),
                            i.1.yellow()
                        );

                        pos += 1;
                    }
                }
            } else if command[0] == calls[6].0 {
                if on_unix_gnu == true {
                    let proceed = bool_question("Do you have Git installed?");
                    if proceed == true {
                        native_command(
                            format!("cd /home/{}/ && git clone {}.git", username, GIT_REPO_LINK)
                                .as_str(),
                        );
                        native_command(
                            format!("cd /home/{}/rupell/ && cargo build --release", username)
                                .as_str(),
                        );
                        native_command(
                            format!(
                                "sudo mv /home/{}/rupell/target/release/rupell /usr/bin/",
                                username
                            )
                            .as_str(),
                        );
                        native_command(format!("rm -rf /home/{}/rupell/", username).as_str());

                        success("Rupell has been successfully updated/installed! Please restart Rupell to use the new changes!");
                    } else {
                        error("Please install Git before running this command!");
                    }
                } else {
                    error("You are not on a UNIX or UNIX-like system!");
                }
            } else if command[0] == calls[7].0 {
                let vls: Vec<usize> = vec![2];
                let valid = check_length(vls, &command);
                if valid == true {
                    warn(command[1].as_str());
                }
            } else if command[0] == calls[8].0 {
                let vls: Vec<usize> = vec![2];
                let valid = check_length(vls, &command);
                if valid == true {
                    error(command[1].as_str());
                }
            } else if command[0] == calls[9].0 {
                let vls: Vec<usize> = vec![2];
                let valid = check_length(vls, &command);
                if valid == true {
                    success(command[1].as_str());
                }
            } else if command[0] == calls[10].0 {
                let vls: Vec<usize> = vec![3];
                let valid = check_length(vls, &command);
                if valid == true {
                    if !command[1].contains(" ") {
                        aliases.push((command[1].to_string(), command[2].to_string()));
                    } else {
                        error("Invalid alias!");
                    }
                }
            } else if command[0] == calls[11].0 {
                let vls: Vec<usize> = vec![1];
                let valid = check_length(vls, &command);
                if valid == true {
                    let mut pos: usize = 1;
                    for i in aliases.iter() {
                        println!(
                            "  {}{} {}{} {}",
                            pos.to_string().cyan().bold(),
                            ".".truecolor(100, 100, 100),
                            i.0.blue(),
                            ":".truecolor(100, 100, 100),
                            i.1.magenta()
                        );

                        pos += 1;
                    }
                }
            }
            // Handling
            else if user_input == "" {
                // Pass
            } else {
                // Smart Error
                let mut void = true;
                let mut proceed = true;

                // ---- aliases ----
                for i in aliases.iter() {
                    if command[0] == i.0 {
                        proceed = false;
                        break;
                    }
                }

                for i in aliases.iter() {
                    if command[0] == i.0 {
                        script.1.push(format!(
                            "{} {}",
                            i.1,
                            string_vec_to_string(&command, " ", 1, command.len())
                        ));
                        script.0 = true;
                    }
                }
                // -----------------

                for i in special_calls.iter() {
                    if command[0] == i.0 {
                        proceed = false;
                        break;
                    }
                }

                if proceed == true {
                    for i in calls.iter() {
                        if i.0 == command[0] {
                            void = false;
                            break;
                        }
                    }
                }

                if proceed == true {
                    if void == true {
                        error("That command is not recognized!");
                    } else {
                        error("That command has an entry, but has nothing tied to it!");
                    }
                }

                // Special Commands
                if command[0] == special_calls[0].0 {
                    if command.len() == 1 {
                        error("That command takes 1 or more arguments!");
                    } else {
                        let phrase = string_vec_to_string(&command, " ", 1, command.len());
                        native_command(phrase.as_str());
                    }
                } else if command[0] == special_calls[1].0 {
                    let vls: Vec<usize> = vec![2];
                    let valid = check_length(vls, &command);
                    if valid == true {
                        let file_name = &format!("{}.rup", command[1]);

                        // Verify if file is there so the program doesn't crash.
                        let success: bool = match File::open(file_name) {
                            Ok(_o) => true,
                            Err(_e) => false,
                        };

                        if success == true {
                            // Open script.
                            let mut file = File::open(file_name)
                                .expect("could not open file because of bad programming");
                            let mut data_string = String::new();
                            file.read_to_string(&mut data_string)
                                .expect("failed to read file");

                            // Push to script.
                            for i in data_string.trim().split("\n") {
                                script.1.push(i.to_string());
                            }

                            // Set up script settings.
                            let answer =
                                bool_question("Would you like to see all commands that run?");
                            if answer == true {
                                script.3 = true;
                            } else {
                                script.3 = false;
                            }
                            script.0 = true;
                            script.2 = 0;
                        } else {
                            error("Could not find or open file!");
                        }
                    }
                }
                // Handling an error.
                else {
                    for i in special_calls.iter() {
                        if command[0] == i.0 {
                            error("That special call has an entry, but has nothing tied to it!");

                            break;
                        }
                    }
                }
            }
        } else {
            error("Invalid command syntax!");
        }
    }
}

pub fn bool_question(text: &str) -> bool {
    loop {
        let answer = input(
            format!(
                "{} {}{}",
                random_color(text),
                random_color("[y/n]"),
                ":".truecolor(100, 100, 100)
            )
            .as_str(),
            true,
        )
        .to_lowercase();

        if answer == "y" {
            return true;
        } else if answer == "n" {
            return false;
        } else {
            error("That is not a valid response! Please try again!");
        }
    }
}
