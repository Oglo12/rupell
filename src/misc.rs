/*
Rupell - A pretty terminal wrapper built in Rust.
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use colored::Colorize;
use users::get_current_username;

pub fn check_length(lengths: Vec<usize>, command: &Vec<String>) -> bool {
    let mut min: &usize = &usize::MAX;
    let mut max: &usize = &0;
    let mut valid = false;

    if lengths.len() == 1 {
        min = &lengths[0];
        max = &lengths[0];
    } else {
        for i in lengths.iter() {
            if i < &min {
                min = i;
            } else if i > &max {
                max = i;
            }
        }
    }

    // Valid?
    if &command.len() < min {
        error("You did not enter enough arguments!");
    } else if &command.len() > max {
        error("You entered too many arguments!");
    } else {
        for i in lengths.iter() {
            if &command.len() == i {
                valid = true;
                break;
            }
        }

        if valid == false {
            error("You did not enter a valid number of arguments!");
        }
    }

    return valid;
}

pub fn error(text: &str) {
    println!(
        "{}{} {}",
        "ERROR".truecolor(255, 150, 150),
        ":".truecolor(100, 100, 100),
        text.truecolor(205, 100, 100)
    );
}

pub fn warn(text: &str) {
    println!(
        "{}{} {}",
        "WARNING".truecolor(255, 255, 150),
        ":".truecolor(100, 100, 100),
        text.truecolor(205, 205, 100)
    );
}

pub fn success(text: &str) {
    println!(
        "{}{} {}",
        "SUCCESS".truecolor(150, 255, 150),
        ":".truecolor(100, 100, 100),
        text.truecolor(100, 205, 100)
    );
}

pub fn string_vec_to_string(vec: &Vec<String>, filler: &str, start: usize, end: usize) -> String {
    let mut phrase = String::new();

    for i in start..end {
        phrase.push_str(vec[i].as_str());

        if i != end - 1 {
            phrase.push_str(filler);
        }
    }

    return phrase;
}

pub fn terminal_size() -> (usize, usize) {
    let size: (usize, usize) = term_size::dimensions().unwrap();
    return size;
}

pub fn char_count(text: &str, character: char) -> usize {
    let mut count: usize = 0;

    for i in text.chars() {
        if i == character {
            count += 1;
        }
    }

    return count;
}

// pub fn find_longest_len_in_string_vec(vec: &Vec<String>) -> usize {
//     let mut longest: usize = 0;

//     for i in vec.iter() {
//         if i.len() > longest {
//             longest = i.len();
//         }
//     }

//     return longest;

pub fn get_username_unix_gnu() -> String {
    let username: String = match get_current_username() {
        Some(uname) => uname.to_str().unwrap().to_string(),
        None => {
            panic!("missing username");
        }
    };

    return username;
}
