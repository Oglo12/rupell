/*
Rupell - A pretty terminal wrapper built in Rust.
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fs;
use std::fs::File;
use std::io::Write;

pub fn populate_read_to_string(file_name: &str, create_data: &str) -> String {
    match File::open(file_name) {
        Ok(_o) => {
            // Just do a normal return!
        }
        Err(_e) => {
            let mut file = File::create(file_name).expect("failed to create file");
            file.write_all(create_data.as_bytes())
                .expect("failed to write");
        }
    };

    let data = fs::read_to_string(file_name)
        .expect("failed to read file")
        .to_string();
    return data;
}
